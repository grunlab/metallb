[![App Status](https://argo-cd-status-badge.grunlab.net/badge?name=metallb&revision=true&showAppName=true)](https://argo-cd.lan.grunlab.net/applications/metallb)

# GrunLab MetalLB

MetalLB deployment on Kubernetes.

Docs: https://docs.grunlab.net/install/metallb.md

GrunLab project(s) using this service:
- [grunlab/decimation][decimation]
- [grunlab/papermc][papermc]
- [grunlab/storj][storj]
- [grunlab/traefik][traefik]

[decimation]: <https://gitlab.com/grunlab/decimation>
[papermc]: <https://gitlab.com/grunlab/papermc>
[storj]: <https://gitlab.com/grunlab/storj>
[traefik]: <https://gitlab.com/grunlab/traefik>